import {callApi} from '../helpers/apiHelper';
import {promiseValue} from "../helpers/apiHelper";
import {fighterValues} from "../helpers/mockData";

class FighterService {
    async getFighters(): Promise<fighterValues[]> {
        try {
            const endpoint: string = 'fighters.json';
            return await callApi(endpoint, 'GET');
        } catch (error) {
            throw error;
        }
    }

    async getFighterDetails(id:string): Promise<promiseValue> {
        try {
            const endpoint = `details/fighter/${id}.json`;
            return await callApi(endpoint, 'GET');
        } catch (error) {
            throw error;
        }
        // todo: implement this method
        // endpoint - `details/fighter/${id}.json`;
    }
}

export const fighterService = new FighterService();
