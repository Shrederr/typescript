import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import {fighterValues} from "../helpers/mockData";


export function createFighters(fighters:  fighterValues[]):HTMLElement {
  const selectFighter= createFightersSelector();
  const container:HTMLElement = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview:HTMLElement = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList:HTMLElement = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements:HTMLElement[] = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: fighterValues, selectFighter: { (event: string, fighterId: string): Promise<void>; (arg0: any, arg1: string): any; }):HTMLElement {
  const fighterElement:HTMLElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement:HTMLElement = createImage(fighter);
  const onClick = (event:any) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter:fighterValues):HTMLElement {
  const { source, name }:{source:string,name:string} = fighter;
  const attributes:{[key:string]:string;} = {
    src: source,
    title: name,
    alt: name,
  };
  return createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes,
  });
}
