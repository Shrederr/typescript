import {showModal} from './modal';
import App from '../../app';
import {createElement} from '../../helpers/domHelper';
import { fighterDetailsValues } from '../../helpers/mockData';

export function showWinnerModal(fighter: fighterDetailsValues|null): void {
    let divWinner: HTMLElement = createElement({
        tagName: 'div',
    });
    if(!fighter){
        return;
    }
    let imgWinner: HTMLElement = createElement({
        tagName: 'img',
        attributes: {
            src: fighter.source,
        },
    });
    divWinner.append(imgWinner);

    function onClose(): void|null {
        const arena: HTMLElement | null = document.querySelector('.arena___root')

        arena?.remove();
        divWinner.remove();
        document.querySelector('link')?.remove();
        new App();
    }

    showModal({title: 'You are winner' + '  ' + fighter.name, bodyElement: divWinner, onClose});
    // call showModal function
}
