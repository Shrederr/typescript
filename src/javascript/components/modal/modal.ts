import {createElement} from '../../helpers/domHelper';

interface showModalValue {
    title: string;
    bodyElement: HTMLElement;
    onClose: object;
}

export function showModal({title, bodyElement, onClose = () => void {}}: showModalValue): void {
    const root: HTMLElement | null = getModalContainer();
    const modal: HTMLElement = createModal({title, bodyElement, onClose});

  root? root.append(modal): null;
}

function getModalContainer(): HTMLElement|null {
    return document.getElementById('root');
}

function createModal({title, bodyElement, onClose}: showModalValue): HTMLElement {
    const layer: HTMLElement = createElement({tagName: 'div', className: 'modal-layer'});
    const modalContainer: HTMLElement = createElement({tagName: 'div', className: 'modal-root'});
    const header: HTMLElement = createHeader(title, onClose);

    modalContainer.append(header, bodyElement);
    layer.append(modalContainer);

    return layer;
}

function createHeader(title: string, onClose: any): HTMLElement {
    const headerElement: HTMLElement = createElement({tagName: 'div', className: 'modal-header'});
    const titleElement: HTMLElement = createElement({tagName: 'span'});
    const closeButton: HTMLElement = createElement({tagName: 'div', className: 'close-btn'});

    titleElement.innerText = title;
    closeButton.innerText = '×';

    const close = (): void => {
        hideModal();
        onClose();
    };
    closeButton.addEventListener('click', close);
    headerElement.append(titleElement, closeButton);

    return headerElement;
}

function hideModal(): void {
    const modal: Element = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
}
