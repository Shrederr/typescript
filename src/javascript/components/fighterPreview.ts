import {createElement} from '../helpers/domHelper';
import {fighterDetailsValues} from "../helpers/mockData";

type fighterValuer = fighterDetailsValues | undefined;

export function createFighterPreview(fighter: fighterValuer, position: string): HTMLElement {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });
    // todo: show fighter info (image, name, health, etc.)
    if (!fighter) {
        return fighterElement;
    }
    const statesElement: HTMLElement = createElement({tagName: 'div', className: 'preview-states'});
    const nameImg: HTMLElement = createElement({tagName: 'div', className: 'preview-states___name'});
    nameImg.innerText = fighter.name;
    statesElement.append(nameImg);
    const statesElementBlock: HTMLElement = createElement({tagName: 'div', className: 'preview-states___container'});
    statesElement.append(statesElementBlock);
    const statesElementBlockNumbers: HTMLElement = createElement({
        tagName: 'div',
        className: 'preview-states___container_numbers'
    });
    statesElementBlock.append(statesElementBlockNumbers);
    const statesElementBlockImage: HTMLElement = createElement({
        tagName: 'div',
        className: 'preview-states___container_images'
    });
    statesElementBlock.append(statesElementBlockImage);
    const healthImgBlock: HTMLElement = createElement({tagName: 'div', className: 'preview-states___health'});
    const healthImg: HTMLElement = createElement({tagName: 'div'});
    const healthNumber: HTMLElement = createElement({tagName: 'div', className: 'preview-states-health___number'});
    healthImg.style.backgroundImage = "url('../../resources/health.png')";
    fighter.health === 60 ? (healthImg.style.width = '100%') : (healthImg.style.width = '60%');
    healthImg.style.height = '100%';
    healthNumber.innerText = String(fighter.health);
    statesElementBlockNumbers.append(healthNumber);
    healthImgBlock.append(healthImg);
    const attackImgBlock: HTMLElement = createElement({tagName: 'div', className: 'preview-states___attack'});
    const attackImg: HTMLElement = createElement({tagName: 'div'});
    const attackNumber: HTMLElement = createElement({tagName: 'div', className: 'preview-states-attack___number'});
    attackImg.style.backgroundImage = "url('../../resources/attack.png')";
    attackImg.style.width = `${fighter.attack * 20 + '%'}`;
    attackImg.style.height = '100%';
    attackNumber.innerText = String(fighter.attack);
    statesElementBlockNumbers.append(attackNumber);
    attackImgBlock.append(attackImg);
    const defenseImgBlock: HTMLElement = createElement({tagName: 'div', className: 'preview-states___defense'});
    const defenseImg: HTMLElement = createElement({tagName: 'div'});
    const defendNumber: HTMLElement = createElement({tagName: 'div', className: 'preview-states-defend___number'});
    defenseImg.style.backgroundImage = "url('../../resources/defense.png')";
    defenseImg.style.width = `${fighter.defense * 20 + '%'}`;
    defenseImg.style.height = '100%';
    defendNumber.innerText = String(fighter.defense);
    statesElementBlockNumbers.append(defendNumber);
    defenseImgBlock.append(defenseImg);
    statesElementBlockImage.append(healthImgBlock);
    statesElementBlockImage.append(attackImgBlock);
    statesElementBlockImage.append(defenseImgBlock);
    fighterElement.append(createFighterImage(fighter));
    fighterElement.append(statesElement);
    return fighterElement;
}

export function createFighterImage(fighter: fighterDetailsValues):HTMLElement {
    const {source, name}:{source:string,name:string} = fighter;
    const attributes:{[key:string]:string;} = {
        src: source,
        title: name,
        alt: name,
    };
    return createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });
}
