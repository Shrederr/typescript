import {createElement} from '../helpers/domHelper';
import {createFighterImage} from './fighterPreview';
import {fight} from './fight';
import {showWinnerModal} from './modal/winner';
import {fighterDetailsValues} from "../helpers/mockData";

export function renderArena(selectedFighters: [playerOne?: fighterDetailsValues | undefined, playerTwo?: fighterDetailsValues | undefined]): void {
    document.head.append(
        createElement({
            tagName: 'link',
            attributes: {
                rel: 'icon',
                href: 'data:;base64,iVBORw0KGgo=',
            },
        })
    );
    const root: HTMLElement | null = document.getElementById('root');
    const arena: HTMLElement | undefined = createArena(selectedFighters);
    if (!root || !arena||!selectedFighters[0] || !selectedFighters[1]) {
        return;
    }

    root.innerHTML = '';
    root.append(arena);

    const fighting: Promise<fighterDetailsValues|null> = fight(selectedFighters[0], selectedFighters[1]);
    fighting.then((result) => showWinnerModal(result));
    // todo:
    // - start the fight
    // - when fight is finished show winner
}

function createArena(selectedFighters: [playerOne?: fighterDetailsValues, playerTwo?: fighterDetailsValues]): HTMLElement | undefined {
    const arena: HTMLElement = createElement({tagName: 'div', className: 'arena___root'});
    const healthIndicators: HTMLElement | undefined = createHealthIndicators(...selectedFighters);
    const fighters: HTMLElement | undefined = createFighters(...selectedFighters);
    if (!healthIndicators || !fighters) {
        return;
    }
    arena.append(healthIndicators, fighters);
    return arena;
}

function createHealthIndicators(leftFighter: fighterDetailsValues | undefined, rightFighter: fighterDetailsValues | undefined): HTMLElement | undefined {
    const healthIndicators: HTMLElement = createElement({tagName: 'div', className: 'arena___fight-status'});
    const versusSign: HTMLElement = createElement({tagName: 'div', className: 'arena___versus-sign'});
    if (!leftFighter || !rightFighter) {
        return undefined;
    }
    const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, 'left');
    const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, 'right');

    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
}

function createHealthIndicator(fighter: fighterDetailsValues, position: string): HTMLElement {
    const {name}: { name: string; } = fighter;
    const container: HTMLElement = createElement({tagName: 'div', className: 'arena___fighter-indicator'});
    const fighterName: HTMLElement = createElement({tagName: 'span', className: 'arena___fighter-name'});
    const indicator: HTMLElement = createElement({tagName: 'div', className: 'arena___health-indicator'});
    const bar: HTMLElement = createElement({
        tagName: 'div',
        className: 'arena___health-bar',
        attributes: {id: `${position}-fighter-indicator`},
    });

    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);

    return container;
}

function createFighters(firstFighter: fighterDetailsValues | undefined, secondFighter: fighterDetailsValues | undefined): HTMLElement | undefined {
    const battleField: HTMLElement = createElement({tagName: 'div', className: `arena___battlefield`});
    if (!firstFighter || !secondFighter) {
        return undefined;
    }
    const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
    const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
}

function createFighter(fighter: fighterDetailsValues, position: string): HTMLElement {
    const imgElement = createFighterImage(fighter);
    const positionClassName: string = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
    const fighterElement: HTMLElement = createElement({
        tagName: 'div',
        className: `arena___fighter ${positionClassName}`,
    });

    fighterElement.append(imgElement);
    return fighterElement;
}
