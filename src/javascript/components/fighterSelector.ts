import {createElement} from '../helpers/domHelper';
import {renderArena} from './arena';
import {createFighterPreview} from './fighterPreview';
import {fighterService} from '../services/fightersService';
import {promiseValue} from "../helpers/apiHelper";
import {fighterDetailsValues} from "../helpers/mockData";

export function createFightersSelector():(event: string, fighterId: string) => Promise<void> {
    let selectedFighters:[playerOne?:fighterDetailsValues, playerTwo?:fighterDetailsValues] = [];
    return async (event, fighterId:string) => {
        const fighter:fighterDetailsValues = await getFighterInfo(fighterId);
        const [playerOne, playerTwo] = selectedFighters;
        const firstFighter:fighterDetailsValues = playerOne ?? fighter;

        const secondFighter:fighterDetailsValues|undefined = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
        selectedFighters = [firstFighter, secondFighter];

        renderSelectedFighters(selectedFighters);
    };
}

const fighterDetailsMap:Map<any,any> = new Map();

export async function getFighterInfo(fighterId:string):Promise<fighterDetailsValues>{
    if (fighterDetailsMap.has(fighterId)) {
        return fighterDetailsMap.get(fighterId);
    } else {
        const fighterDetails:promiseValue = await fighterService.getFighterDetails(fighterId);
        fighterDetailsMap.set(fighterId, fighterDetails);
        return fighterDetailsMap.get(fighterId);
    }

    // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
}

function renderSelectedFighters(selectedFighters:[playerOne?:fighterDetailsValues, playerTwo?:fighterDetailsValues]):void {
    const fightersPreview:HTMLElement | null = document.querySelector('.preview-container___root');
    const [playerOne, playerTwo]:[playerOne?:fighterDetailsValues, playerTwo?:fighterDetailsValues] = selectedFighters;
    const firstPreview:HTMLElement = createFighterPreview(playerOne, 'left');
    const secondPreview:HTMLElement = createFighterPreview(playerTwo, 'right');
    const versusBlock:HTMLElement = createVersusBlock(selectedFighters);
    if(!fightersPreview){
        return;
    }
    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters:[playerOne?:fighterDetailsValues, playerTwo?:fighterDetailsValues]):HTMLElement {
    const canStartFight = selectedFighters.filter(Boolean).length === 2;
    const onClick = () => startFight(selectedFighters);
    const container = createElement({tagName: 'div', className: 'preview-container___versus-block'});
    const image = createElement({
        tagName: 'img',
        className: 'preview-container___versus-img',
        attributes: {src: '../../../resources/versus.png'},
    });
    const disabledBtn = canStartFight ? '' : 'disabled';
    const fightBtn = createElement({
        tagName: 'button',
        className: `preview-container___fight-btn ${disabledBtn}`,
    });

    fightBtn.addEventListener('click', onClick, false);
    fightBtn.innerText = 'Fight';
    container.append(image, fightBtn);

    return container;
}

function startFight(selectedFighters:[playerOne?:fighterDetailsValues, playerTwo?:fighterDetailsValues]) {
    renderArena(selectedFighters);
}
