import {controls} from '../../constants/controls';
import {Animation} from '../helpers/animationHelper';
import {fighterDetailsValues} from "../helpers/mockData";

interface pressKeyValue{
    [key:string]:boolean;
}
export async function fight(firstFighter:fighterDetailsValues, secondFighter:fighterDetailsValues):Promise<fighterDetailsValues|null> {
    return new Promise((resolve) => {
        const blockSound:HTMLAudioElement = new Audio('../../resources/sounds/block.mp3');
        const attackSound:HTMLAudioElement = new Audio('../../resources/sounds/attack.mp3');
        const criticalSound:HTMLAudioElement = new Audio('../../resources/sounds/critical.mp3');
        const healthPlayerOne:HTMLElement | null = document.getElementById('left-fighter-indicator');
        const healthPlayerTwo:HTMLElement | null = document.getElementById('right-fighter-indicator');
        let firstFighterHealth:number = firstFighter.health;
        let secondFighterHealth:number = secondFighter.health;
        const resetTime:number = 10000;
        let time:number = new Date().getTime() - resetTime;
        let time2:number = new Date().getTime() - resetTime;
        const pressKey:pressKeyValue = {};
        for (let key in controls) {
            if (controls.hasOwnProperty(key)) {
                if (controls[key] === controls.PlayerOneCriticalHitCombination) {
                    controls.PlayerOneCriticalHitCombination.forEach((element: string) => {
                        pressKey[element] = false;
                    });
                } else if(controls[key] === controls.PlayerTwoCriticalHitCombination){
                    controls.PlayerTwoCriticalHitCombination.forEach((element: string) => {
                        pressKey[element] = false;
                    });
                } else{
                    pressKey[controls[key].toString()] = false;
                }
            }
        }
        const arrayOfKey:Array<string> = Object.keys(pressKey);

        function handle(event: { code: string; }):HTMLElement|undefined {
            if (
                arrayOfKey.includes(event.code) &&
                event.code !== controls.PlayerOneAttack &&
                event.code !== controls.PlayerTwoAttack
            ) {
                pressKey[event.code] = true;
            }

            if (event.code === controls.PlayerOneAttack && pressKey[controls.PlayerOneAttack]) {
                return;
            }
            if (event.code === controls.PlayerOneAttack && !pressKey[controls.PlayerOneBlock]) {
                if (pressKey[controls.PlayerTwoBlock]) {
                    Animation('right', blockSound, false, true);
                    secondFighterHealth -= getDamage(firstFighter, secondFighter, true);
                    pressKey[controls.PlayerOneAttack] = true;
                } else if (!pressKey[controls.PlayerTwoBlock]) {
                    attackSound.play();
                    secondFighterHealth -= getDamage(firstFighter, secondFighter, false);
                    pressKey[controls.PlayerOneAttack] = true;
                }
            }
            if (
                pressKey[controls.PlayerOneCriticalHitCombination[0]] &&
                pressKey[controls.PlayerOneCriticalHitCombination[1]] &&
                pressKey[controls.PlayerOneCriticalHitCombination[2]] &&
                !pressKey[controls.PlayerOneAttack] &&
                !pressKey[controls.PlayerOneBlock]
            ) {
                let BoostTime:number = new Date().getTime();
                if (BoostTime >= time + resetTime) {
                    Animation('left', criticalSound, true, false);
                    secondFighterHealth -= firstFighter.attack * 2;
                    time = BoostTime;
                }
            }
            if (event.code === controls.PlayerTwoAttack && pressKey[controls.PlayerTwoAttack]) {
                return;
            }
            if (event.code === controls.PlayerTwoAttack && !pressKey[controls.PlayerTwoBlock]) {
                if (pressKey[controls.PlayerOneBlock]) {
                    Animation('left', blockSound, false, true);
                    firstFighterHealth -= getDamage(secondFighter, firstFighter, true);
                    pressKey[controls.PlayerTwoAttack] = true;
                } else if (!pressKey[controls.PlayerOneBlock]) {
                    attackSound.play();
                    firstFighterHealth -= getDamage(secondFighter, firstFighter, false);
                    pressKey[controls.PlayerTwoAttack] = true;
                }
            }
            if (
                pressKey[controls.PlayerTwoCriticalHitCombination[0]] &&
                pressKey[controls.PlayerTwoCriticalHitCombination[1]] &&
                pressKey[controls.PlayerTwoCriticalHitCombination[2]] &&
                !pressKey[controls.PlayerTwoAttack] &&
                !pressKey[controls.PlayerTwoBlock]
            ) {
                let BoostTime2:number = new Date().getTime();
                if (BoostTime2 >= time2 + resetTime) {
                    Animation('right', criticalSound, true, false);
                    firstFighterHealth -= secondFighter.attack * 2;
                    time2 = BoostTime2;
                }
            }
            if(!healthPlayerTwo || !healthPlayerOne){
                return;
            }
            healthPlayerTwo.style.width = (secondFighterHealth / secondFighter.health) * 100 + '%';
            healthPlayerOne.style.width = (firstFighterHealth / firstFighter.health) * 100 + '%';
            if (secondFighterHealth <= 0 || firstFighterHealth <= 0) {
                firstFighterHealth <= 0 ? (healthPlayerOne.style.width = '0%') : (healthPlayerTwo.style.width = '0%');
                document.removeEventListener('keydown', handle);
                document.removeEventListener('keyup', (event) => {
                    if (arrayOfKey.includes(event.code)) {
                        pressKey[event.code] = false;
                    }
                });

                firstFighterHealth <= 0 ? resolve(secondFighter) : resolve(firstFighter);
            }
        }

        document.addEventListener('keydown', handle);
        document.addEventListener('keyup', (event) => {
            if (arrayOfKey.includes(event.code)) {
                pressKey[event.code] = false;
            }
        });
    });
    // resolve the promise with the winner when fight is over
}

export function getDamage(attacker:fighterDetailsValues, defender:fighterDetailsValues, isBlocked:boolean):number {
    if (isBlocked) {
        return 0;
    }
    const damage:number = getHitPower(attacker);
    const block:number = getBlockPower(defender);
    return damage - block > 0 ? damage - block : 0;
}

export function getHitPower(attacker:fighterDetailsValues):number {
    // return hit power
    return attacker.attack * (Math.random() + 1);
}

export function getBlockPower(defender:fighterDetailsValues):number {
    // return block power
    return defender.defense * (Math.random() + 1);
}
