interface createElementValue{
    tagName: string;
    className?: string;
    attributes?: {
        [key:string]:string;
    };
}

export function createElement({tagName, className, attributes = {}}: createElementValue): HTMLElement|HTMLAudioElement {
    const element:HTMLElement = document.createElement(tagName);

    if (className) {
        const classNames: Array<string> = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

    return element;
}
