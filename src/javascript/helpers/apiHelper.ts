import {fightersDetails, fighters} from './mockData';
import {fighterDetailsValues, fighterValues} from "./mockData";

export type promiseValue = Array<fighterValues> | fighterDetailsValues[] ;
const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

interface optionsValue {
    method: string;
}

async function callApi(endpoint: string, method: string): Promise<promiseValue> {
    const url: string = API_URL + endpoint;
    const options: optionsValue = {
        method,
    };

    return useMockAPI
        ? fakeCallApi(endpoint)
        : fetch(url, options)
            .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
            .then((result) => JSON.parse(atob(result.content)))
            .catch((error) => {
                throw error;
            });
}

async function fakeCallApi(endpoint: string): Promise<fighterValues[] | fighterDetailsValues | Error> {
    const response: Array<fighterValues> | fighterDetailsValues | undefined = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

    return new Promise((resolve, reject) => {
        setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
    });
}

function getFighterById(endpoint: string): fighterDetailsValues|undefined {
    const start: number = endpoint.lastIndexOf('/');
    const end: number = endpoint.lastIndexOf('.json');
    const id: string = endpoint.substring(start + 1, end);

    return fightersDetails.find((it) => it._id === id);
}

export {callApi};
