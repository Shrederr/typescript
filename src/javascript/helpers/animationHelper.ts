import { createElement } from './domHelper';

export function Animation(position:string, sound:HTMLAudioElement, critical:boolean, block:boolean):void {
  const arena:HTMLElement | null = document.querySelector('.arena___root');
  if(!arena){
    return;
  }
  if (critical) {
    let criticalImg:HTMLElement = createElement({
      tagName: 'img',
      className: `animation___critical-${position}`,
      attributes: {
        src: '../../resources/critical.png',
      },
    });
    arena.append(criticalImg)
    let width:number = 150;
    let start:number = Date.now();
    sound.play();
    setInterval(function () {
      let timePassed:number = Date.now() - start;
      width += 1;
      let opacityValue:string = criticalImg.style.opacity;
      criticalImg.style.opacity = Number(opacityValue) - 0.01 + '';
      criticalImg.style.width = width + 'px';
      if (timePassed > 2000) criticalImg.remove();
    }, 20);
  }
  if (block) {
    let blockImg:HTMLElement = createElement({
      tagName: 'img',
      className: `animation___block-${position}`,
      attributes: {
        src: '../../resources/block.png',
      },
    });
    arena.append(blockImg);
    let top:number = 50;
    let start:number = Date.now();
    sound.play();
    setInterval(function () {
      let timePassed:number = Date.now() - start;
      top -= 1;
      let opacityValue:number = Number(blockImg.style.opacity);
      blockImg.style.opacity = opacityValue - 0.1 + '';
      blockImg.style.top = top + '%';
      if (timePassed > 500) blockImg.remove();
    }, 40);
  }
}
