interface controlsValue {
[key:string]:string|string[];
  PlayerOneAttack:string;
  PlayerOneBlock: string;
  PlayerTwoAttack: string;
  PlayerTwoBlock: string;
  PlayerOneCriticalHitCombination: Array<string>;
  PlayerTwoCriticalHitCombination: Array<string>;
}

export const controls:controlsValue = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO'],
};
